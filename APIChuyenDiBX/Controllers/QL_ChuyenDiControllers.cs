﻿using APIChuyenDiBX.Models;
using APIChuyenDiBX.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIChuyenDiBX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QL_ChuyenDiControllers : ControllerBase
    {

        private readonly QL_ChuyenDiServices _chuyenDiService;
        public QL_ChuyenDiControllers(QL_ChuyenDiServices chuyenDiService)
        {
            _chuyenDiService = chuyenDiService;
        }
        //Cau1
        [HttpPost("ThemChuyenDi")]
        public IActionResult ThemChuyenDi([FromBody] ThemChuyen input)
        {
            return Ok(_chuyenDiService.ThemChuyenDi(input));
        }
        //Cau2
        [HttpPut("Chuyen_Trang_Thai/" + "{id}")]
        public IActionResult ChuyenTrangThai(Guid id)
        {
            return Ok(_chuyenDiService.ChuyenTrangThai(id));
        }
        //Cau3
        [HttpPut("Huy_Chuyen_Di/" + "{id}")]
        public IActionResult HuyChuyen(Guid id, HuyChuyenDi input)
        {
            return Ok(_chuyenDiService.HuyChuyen(id, input));
        }
        //Cau4
        [HttpGet("DS_Chuyen_Di_Tu_Ben_TN")]
        public IActionResult DSChuyendiHoanThanh()
        {
            return Ok(_chuyenDiService.DSChuyendiHoanThanh());
        }
        //Cau5
        [HttpGet("DS_Chuyen_Di_Huy")]
        public IActionResult DSChuyenHuy()
        {
            return Ok(_chuyenDiService.DSChuyenHuy());
        }
    }
}
