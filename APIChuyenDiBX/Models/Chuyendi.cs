﻿using System;
using System.Collections.Generic;

namespace APIChuyenDiBX.Models
{
    public partial class Chuyendi
    {
        public Chuyendi()
        {
            ChuyendiNhanviens = new HashSet<ChuyendiNhanvien>();
        }

        public Guid IdChuyenDi { get; set; }
        public Guid IdTuyenDuong { get; set; }
        public Guid IdTrangThai { get; set; }
        public string MaChuyenDi { get; set; } = null!;
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string? LyDoHuy { get; set; }
        public DateTime? ThoiGianHuy { get; set; }

        public virtual ChuyendiThongtinphuongtien ChuyendiThongtinphuongtien { get; set; } = null!;
        public virtual Trangthai IdTrangThaiNavigation { get; set; } = null!;
        public virtual Tuyenduong IdTuyenDuongNavigation { get; set; } = null!;
        public virtual ICollection<ChuyendiNhanvien> ChuyendiNhanviens { get; set; }
    }
}
