﻿using System;
using System.Collections.Generic;

namespace APIChuyenDiBX.Models
{
    public partial class Huyen
    {
        public Huyen()
        {
            Benxes = new HashSet<Benxe>();
        }

        public Guid IdHuyen { get; set; }
        public string TenHuyen { get; set; } = null!;

        public virtual ICollection<Benxe> Benxes { get; set; }
    }
}
