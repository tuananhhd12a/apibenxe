﻿using System;
using System.Collections.Generic;

namespace APIChuyenDiBX.Models
{
    public partial class Benxe
    {
        public Benxe()
        {
            TuyenduongIdBenDenNavigations = new HashSet<Tuyenduong>();
            TuyenduongIdBenXuatPhatNavigations = new HashSet<Tuyenduong>();
        }

        public Guid IdBenXe { get; set; }
        public Guid IdTinh { get; set; }
        public Guid IdHuyen { get; set; }
        public string TenBenXe { get; set; } = null!;
        public string? DiaChiCuThe { get; set; }

        public virtual Huyen IdHuyenNavigation { get; set; } = null!;
        public virtual Tinh IdTinhNavigation { get; set; } = null!;
        public virtual ICollection<Tuyenduong> TuyenduongIdBenDenNavigations { get; set; }
        public virtual ICollection<Tuyenduong> TuyenduongIdBenXuatPhatNavigations { get; set; }
    }
}
