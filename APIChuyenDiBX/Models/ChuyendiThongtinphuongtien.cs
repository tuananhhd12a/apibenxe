﻿using System;
using System.Collections.Generic;

namespace APIChuyenDiBX.Models
{
    public partial class ChuyendiThongtinphuongtien
    {
        public Guid IdChuyenDi { get; set; }
        public string TenNhaXe { get; set; } = null!;
        public string SoDienThoaiNhaXe { get; set; } = null!;
        public string BienKiemSoat { get; set; } = null!;
        public int SoGhe { get; set; }
        public int SoGiuong { get; set; }

        
        public virtual Chuyendi IdChuyenDiNavigation { get; set; } = null!;
    }
}
