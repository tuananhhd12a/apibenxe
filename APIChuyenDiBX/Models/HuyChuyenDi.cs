﻿namespace APIChuyenDiBX.Models
{
    public class HuyChuyenDi
    {
        /// <summary>
        /// Lý do hủy chuyến đi
        /// </summary>
        public string? LyDoHuy { get; set; }
    }
}
