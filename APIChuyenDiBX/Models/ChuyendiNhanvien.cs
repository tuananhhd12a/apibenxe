﻿using System;
using System.Collections.Generic;

namespace APIChuyenDiBX.Models
{
    public partial class ChuyendiNhanvien
    {
        public Guid IdNhanVien { get; set; }
        public Guid IdChuyenDi { get; set; }
        public string HoTenNhanVien { get; set; } = null!;

        public virtual Chuyendi IdChuyenDiNavigation { get; set; } = null!;
    }
}
